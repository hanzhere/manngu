import React, { useState, useEffect } from 'react'
import './Content.scss'
import manImage from '../../assets/img/man.png'
import { motion } from 'framer-motion'

const easing = [0.6, -0.05, 0.01, 0.99]

const fadeInUp = {
    initial: {
        y: 60,
        opacity: 0
    },
    animate: {
        y: 0,
        opacity: 1,
        transition: {
            duration: 1,
            delay: 1,
            ease: easing
        }
    }
}

const fadeIn = {
    initial: {
        y: 0,
        opacity: 0
    },
    animate: {
        y: 0,
        opacity: 1,
        transition: {
            duration: 1,
            delay: 1,
            ease: easing
        }
    }
}


function Content() {

    const [position, setPosition] = useState({ x: 0, y: 0 });


    const onMouseMove = (e) => {
        setPosition({ x: e.clientX, y: e.clientY });
    };

    const addEventListeners = () => {
        document.addEventListener("mousemove", onMouseMove);
    };

    const removeEventListeners = () => {
        document.removeEventListener("mousemove", onMouseMove);
    };

    useEffect(() => {
        addEventListeners();
        return () => removeEventListeners();
    }, []);

    return (
        <motion.div className='content' initial='initial' animate='animate' >
            <motion.div initial='initial' animate='animate' className='content__left'>
                <motion.div className='content__hero'>
                    <motion.p variants={fadeInUp} ><motion.a>Go bus my cu</motion.a></motion.p>
                    <motion.p variants={fadeInUp} ><motion.a>you'll never lose</motion.a></motion.p>
                </motion.div>
                <motion.div variants={fadeInUp} className='content__describe'>
                    <span>kajdhfgasdjgfaksdjgfasdkjfg ka<br></br>dhfgasdjgfaksdjgfasdkjfg kajdhfgasdjgfaksdjgfasdkjfg kajdhfgasdjgfaksdjgfasdkjfg</span>
                </motion.div    >
                <motion.div variants={fadeInUp}>
                    <div className='content__contact'>
                        BOOK ME
                </div>
                </motion.div>

            </motion.div>
            <motion.div initial='initial' animate='animate' exit='exit' className='content__right'>
                <img src={manImage} />
                <div className='content__right__circle_container'>
                    <div className='content__right__circle'>
                        <motion.div variants={fadeInUp} className='content__right__circle1' ></motion.div>
                        <motion.div variants={fadeInUp} className='content__right__circle2' ></motion.div>
                        <motion.div variants={fadeInUp} className='content__right__circle3'></motion.div>
                        <motion.div variants={fadeIn} className='content__right__circle4'></motion.div>
                    </div>

                </div>

            </motion.div>

        </motion.div>
    )
}

export default Content
