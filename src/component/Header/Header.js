import React, { useState, useEffect } from 'react'
import './Header.scss'
import { motion } from 'framer-motion'

const easing = [0.6, -0.05, 0.01, 0.99]

const fadeInDown = {
    initial: {
        y: -60,
        opacity: 0
    },
    animate: {
        y: 0,
        opacity: 1,
        transition: {
            duration: 1,
            delay: 1,
            ease: easing
        }
    }
}

const fadeInDown1 = {
    initial: {
        y: -60,
        opacity: 0
    },
    animate: {
        y: 0,
        opacity: 1,
        transition: {
            duration: 1,
            delay: 2,
            ease: easing
        }
    }
}

function Header() {
    return (
        <motion.div initial='initial' animate='animate' className='header'>
            <motion.div variants={fadeInDown} className='header__logo'><a>manngu<span>.</span></a></motion.div>

            <motion.div variants={fadeInDown} className='header__menu'>
                <ul>
                    <motion.li><a href='#'>About<span>.</span></a></motion.li>
                    <motion.li><a href='#'>Service<span>.</span></a></motion.li>
                    <motion.li><a href='#'>Work<span>.</span></a></motion.li>
                </ul>
            </motion.div>

            <a>
                <motion.div variants={fadeInDown} className='header__contact'>
                    Contact
                </motion.div>
            </a>

        </motion.div >
    )
}

export default Header
