import React from 'react';
import './App.scss'
import Content from './component/Content/Content';
import Cursor from './component/Cursor/Cursor';
import Header from './component/Header/Header';
import { motion, AnimatePresence } from 'framer-motion'

const transition = { duration: 1.4, ease: [.6, .01, -.05, .9] }

function App() {



  return (
    <div className="app">
      <AnimatePresence exitBeforeEnter>
        <motion.div
          initial={{ y: 0 }} animate={{ y: "-100vh", transition }}
          // exit={{ y: -200 }}
          className='animate'>
        </motion.div >


        <Cursor />
        <Header />
        <Content />
      </AnimatePresence>
    </div>
  );
}

export default App;
